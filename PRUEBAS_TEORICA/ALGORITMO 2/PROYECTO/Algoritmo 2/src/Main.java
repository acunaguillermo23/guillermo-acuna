public class Main {
    /**
     * Dado un arreglo rotado (por ejemplo, [7, 8, 9, 4, 5, 6]), escribe un programa que encuentre el punto de rotación, es decir, el índice donde el arreglo comienza a rotar.
     * Definición de arreglo rotado
     * Ejemplo #1
     * Arreglo normal = [4,5,6,7,8,9]
     * Arreglo rotado = [7,8,9,4,5,6]
     * Índice rotación: 3
     * Valor punto rotación: 4
     *
     * Ejemplo #2
     * Arreglo normal = [20,30,40,50,60,70,80,90,100]
     * Arreglo rotado = [30,40,50,60,70,80,90,100,20]
     * Índice rotación: 8
     * Valor punto rotación: 20
     * @param args
     */

    public static void main(String[] args) {
        int[] arreglo = {7,8,9,4,5,6};
//        int[] arreglo = {30,40,50,60,70,80,90,100,20};
        Main m = new Main();
        int indice = m.encontrarIndiceRotacion(arreglo);
        System.out.println("Índice de rotación: " + indice);
        System.out.println("Valor punto rotación: " + arreglo[indice]);
    }

    public int encontrarIndiceRotacion(int[] arreglo){
        int temp = 0, indice = 0;
        for(int i = 0; i < arreglo.length; i ++){
            temp = arreglo[i];// 9
            if(arreglo[i+1] > temp) // 4 > 9
                temp = arreglo[i+1]; // tomo el siguiente valor
            else
            {
                indice = i+1; // capturo el indice
                break; // termino la ajecución
            }

        }

        return indice;
    }
}