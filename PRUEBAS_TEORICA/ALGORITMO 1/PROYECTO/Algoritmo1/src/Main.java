import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    /**
     * Crea un programa que genere todos los anagramas posibles de una cadena ingresada por teclado. Por ejemplo, si la cadena es "abc",
     * el programa debe generar "abc", "acb", "bac", "bca", "cab" y "cba".
     * La cadena ingresada tiene que tener las siguientes características
     * Entre 3 y 5 letras
     * Solo permite caracteres alfanuméricos
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String cadena = null;


        do{
            System.out.print("Ingrese la cadena: ");
            cadena = s.nextLine();
        }while ( cadena.length() > 5 || cadena.length() < 3 ); // 5 > 6  $ 5< 4

        s.close();
        Main m = new Main();
        ArrayList<String> resultado = m.obtenerVariantes(cadena);

        int i = 1;
        for ( String x : resultado
        ) {
            System.out.println("Variante " + i + ": " + x);
            i++;
        }

    }
    
    public ArrayList<String> obtenerVariantes(String cadena) { // abc
        ArrayList<String> resultado = new ArrayList<>();
        recursivo(cadena, "", resultado); // abc - "" - []
        return  resultado;
    }


    private void recursivo(String cadena, String valorInicial, ArrayList<String> resultado){
        if(cadena.isEmpty() || cadena.isBlank()){//pregunto si la iteración finalizó para guardar el resultado
            resultado.add(valorInicial); // abc
        }else{
            for(int x = 0; x < cadena.length(); x++){
                Character primeraLetra = cadena.charAt(x); // obtengo el primero caracter
                String cadenaRestante = cadena.substring(0,x) + cadena.substring(x+1);//formo una nueva cadena con los caracteres restantes
                recursivo(cadenaRestante,valorInicial+primeraLetra, resultado); // envio la cadena restante, con la formación del restante y el valor para ir concatenando
            }
        }
    }

}